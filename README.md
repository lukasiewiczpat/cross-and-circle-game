# Cross and circle game

Cross and circle game is a variety of *Five in a Row* game, also called *Gomoku*. The game is a network base one and supports multiplayer mode (two players competition) and single-player mode (one player against a bot). There is also a possibility to organize a bots' fight - details are given below.

## Prerequisites

To run a game Python 3 and Tkinter is required. Official Python distribution is available [here](https://www.python.org/ "Python wabsite").

## Quick start

1. Run ```server.py```. The server app reads IP address from network card and waits for a connection on a port 2020. **Be careful:** in a case of many network cards (for example virtual) wrong one can be read. Server IP address is printed after server has started. If it's incorrect, turn off temporarily network cards which you don't want to use and start ```server.py``` again. There is no need to restart the server app between games.

1. Run ```client_manual.py```, type server IP address and player name. When you accept player name, a tkinter window (a board) opens. During a game all information are displayed in a terminal. When the game ends and you close the tkinter window, the application closes automatically. It is possible to set static IP address in line 448:

```python
#INPUT server address and username
ip_address_server=str(input("server ip address: "))    
username=str(input("\nusername: "))

# static address
#ip_address_server="192.168.0.129"#temp
```

3. Run ```client_bot.py``` and enter server IP address for use single-player mode. **OR** Run ```client_manual.py``` on a second PC for use multiplayer mode. The bot runs automatically and it closes window when a game end. It is possible to set static IP for the bot client in line 272:

```python
#INPUT server address and username
ip_address_server=str(input("server ip address: "))

# static address
#ip_address_server="192.168.0.129"#temp
```

## client_bot.py algorithm description

The bot client application is similar to ```client_manual.py```. It contains structure which allows to cooperate with the server application and conducts games. The main difference between client applications is a large algorithm which is attached to the bot version and calculates shots coordinates. The self-made algorithm consists of two parts:

* Deterministic defensive part which scores and classifies cells of the board as dangerous basing on bot's and bot's opponent's shots.

* Predictive offensive part which looks for quickest possibilities to win the game.

## Bots' fight

Do you think you may develop a better shooting algorithm? It's possible to check if you are right. Use ```client_bot_empty.py``` and put your algorithm inside shot method:

```python
    def shot(self, board, sign):
        '''
        [...]
        '''

        # --- TEMP ---        
        # Place for algorithm which calculates shot coordinates     
        while 1:
            x=int(round(14*random.random()))
            y=int(round(14*random.random()))
            if board[x][y]==' ':
                break
        # ------------

        return [x, y]
```

## Credits

Special thanks go to [sentdex](https://www.youtube.com/user/sentdex/featured "sentdex YouTube channel") who prepared great socket tutorial. The game uses communication structure shown in YouTube videos.

## License

Cross and circle game is distributed under the MIT License, see the [LICENSE](https://gitlab.com/lukasiewiczpat/cross-and-circle-game/-/blob/master/LICENSE "LICENSE") file.

## Screenshots

### ```server.py```

![server.py](https://gitlab.com/lukasiewiczpat/cross-and-circle-game/-/raw/master/Screenshots/server.png)

### ```client_manual.py```

![client_manual.py](https://gitlab.com/lukasiewiczpat/cross-and-circle-game/-/raw/master/Screenshots/client_manual.png)


### ```client_bot.py```

![client_bot.py](https://gitlab.com/lukasiewiczpat/cross-and-circle-game/-/raw/master/Screenshots/client_bot.png)