import socket
import pickle
import time
import random


#CLASSES
class game:
    def __init__(self):
        '''
        Create game object
        ---
        Attributes:
        _all_states (dictionary): all game states description
        _state (int): actual game state
        '''
        self._all_states={"idle":0, "I'm_ready":1, "my_shot":2, "opponent_shot":3, "end_of_game":4}
        self._state=0


    def change_state(self, new_state):
        '''
        Method which changes state
        ---
        Parameters:
        new_state (int): activate this state
        '''        
        self._state=new_state


    def shot(self, board, sign):
        '''
        Method which calculates x,y shot coordinate based on actual board
        ---
        Parameters:
        board (list): 2-demensional list of strings size 15x15, it is a board for a game
        sign (string): bot sign: 'x' or 'o'
        ---
        Returns:
        [x y] (list): list of two elements - shot coordinate
            x (int): <0;14>
            y (int): <0;14>
        '''

        # --- TEMP ---        
        # Place for algorithm which calculates shot coordinates     
        while 1:
            x=int(round(14*random.random()))
            y=int(round(14*random.random()))
            if board[x][y]==' ':
                break
        # ------------

        return [x, y]



class server:
    def __init__(self, _ip_address, _port):
        '''
        Create server object for communication with a server
        ---
        Attributes:
        _ip_address (string):
        _port (int):
        _client_socket (socket): client socket to send and receive messages
        _all_control (dictionary): all possible control values description
        _server_control (int): last control value received from server
        _server_supp_string (string): last supplementary string value received from server
        _server_board (list): 2-demensional list of strings size 15x15, it is a board received from the server
        '''
        self._ip_address=_ip_address
        self._port=_port
        self._client_socket=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._all_control={"I'm_ready":1, "my_shot":2, "I'm_quitting":3}
        self._server_control=0
        self._server_supp_string=''
        self._server_board=[]


    def set_socket_blockig(self, _bool):
        '''
        Method allows to configure setblocking option for client socket
        ---
        Parameters:
        _bool (bool):   True - activate blocking by the receiving socket
                        False - deactivate blocking by the receiving socket
        '''
        self._client_socket.setblocking(_bool)


    def connect(self):
        '''
        Method connects to server
        ---
        Returns:
        True/False (bool): bool information about connection if it's ok or not
        '''
        try:
            self._client_socket.connect((ip_address_server, port_server))
            return True
        except:
            return False


    def send(self, control=0, supp_string="", shot=[0,0]):
        '''
        Method sends message to server
        ---
        Parameters:
        control (int): integer value specifying message type:
                    1-I'm ready; 2-My shot; 3-I'm quitting
        supp_string (string): string value allows to send supplementary information such player name
        shot (list) -> [x y]: x,y (int)
        '''
        msg=[control, supp_string, shot]
        self._client_socket.send(pickle.dumps(msg))


    def receive(self):
        '''
        Method receives message from server and updates _server_control, _server_supp_string, _server_board
        '''
        msg_recv=pickle.loads(self._client_socket.recv(4096))

        self._server_control=msg_recv[0]
        self._server_supp_string=msg_recv[1]
        self._server_board=msg_recv[2]




if __name__ == "__main__":

    #INPUT server address and username
    ip_address_server=str(input("server ip address: "))

    # static address
    #ip_address_server="192.168.0.129"#temp
    port_server=int(2020)
    username="bot_empty"

    server=server(ip_address_server, port_server)#server init
    connected=server.connect()

    game=game()#game init

    if connected:
        #connected to server
        server.send(supp_string=username)#send first message containing username
        print("\n****Connected to server****")

        game.change_state(game._all_states["idle"])

        while 1:
            #script loop

            if game._state==0:

                time.sleep(0.2)            
                #Send "ready" information and start the game:
                server.send(control=server._all_control["I'm_ready"])    
                #Go to "I'm_ready" state
                game.change_state(game._all_states["I'm_ready"])
                print("\n--- Ready message was sent ---")

            else:
                # others states
                server.receive()#first step is to receive message from server

                # ----- update game._state -----
                if server._server_control==1:
                    game.change_state(game._all_states["I'm_ready"])

                elif server._server_control==2:
                    game.change_state(game._all_states["my_shot"])

                elif server._server_control==3:
                    game.change_state(game._all_states["opponent_shot"])

                elif server._server_control==4 or server._server_control==5:
                    game.change_state(game._all_states["end_of_game"])
                # ------------------------------


                # ----- states reaction -----
                if game._state==1:
                    # "I'm_ready" state
                    print("\n+++ Waiting for opponent +++")

                elif game._state==2:
                    # "my_shot" state
                    print("\n*** Making a shot ***")
                    
                    shot_list=game.shot(server._server_board, server._server_supp_string)
                    time.sleep(1)#wait 1s before shot
                    server.send(control=2, shot=shot_list)

                    print(f"^^^ Bot shot (x,y): ({shot_list[0]},{shot_list[1]}) ^^^")

                elif game._state==3:
                    # "opponent_shot" state
                    print("\n*** Opponent shot ***")

                elif game._state==4:
                    # "end_of_game" state
                    print("\n&&& End of game &&&")

                    if server._server_control==4:
                        print("\n#####   YOU ARE THE WINNER!   #####")
                    elif server._server_control==5:
                        print("\n#####   YOU ARE THE LOSER!   #####")

                    time.sleep(2)                    
                    break
                # ---------------------------

    else:
        #connection error
        print("\nConnection error occured.")

    print("\nQuitting... bye, bye")
    time.sleep(2)