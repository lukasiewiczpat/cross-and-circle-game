import numpy as np

class bot:


    def __init__(self):
        '''
        Create bot with all needed functionality
        ---
        Attributes:
        _act_board (numpy.ndarray): 2-dimensional matrix contains actual board state where: 0-'', 1-'x', 2-'o'
        _prev_board (numpy.ndarray): 2-dimensional matrix contains previous board state where: 0-'', 1-'x', 2-'o'
        _directional_risk_matrix (numpy.ndarray): 3-dimensional matrix -> for each cell contains risk from 8 directions.
                                                                          Risk increases when opponent choose nearby cells and decreases or resets when bot signs appeared. 
                                                                          Directions have clockwise numbers starting from 12 o'clock (y axis is horizontal; x axis is vertical):
                                                                          7  0  1     |---->y
                                                                          6     2     |
                                                                          5  4  3     `x
        _chance_matrix (numpy.ndarray): 3-dimensional matrix -> for each cell contains:
                                        [movements_to_end, number_of_occurrences] -> movements_to_end: 1 - if bot choose this cell then win the game
                                                                                                       2 - this cell and one more is needed to end the game
                                                                                                       9 - none of the above
                                                                                    number_of_occurrences: how many times this cell was recognized as 2 movements to end
                                                                                                           (in case of 1 movement to end, it doesn't matter)                                                                                                                    
        _directional_neighborhood_matrix (numpy.ndarray): 3-dimensional matrix -> for each cell contains neighborhood information for each direction:
                                                                                  0 - there isn't opponent sign next to cell on appriopriate direction
                                                                                  1 - there is opponent sign next to cell on appriopriate direction
                                                                                  Directions have numbers analogous to _directional_risk_matrix.                                                                    
        _bot_neighborhood_matrix (numpy.ndarray): 2-dimensional matrix -> for each cell gives information if bot sign is next to cell (directions are disregarded)  
        '''
        self._act_board=np.zeros((15, 15), dtype=int)
        self._prev_board=np.zeros((15, 15), dtype=int)
        self._directional_risk_matrix=np.zeros((15, 15, 8), dtype=int)
        self._directional_neighborhood_matrix=np.zeros((15, 15, 8), dtype=int)
        self._chance_matrix=np.ones((15, 15, 2), dtype=int)*9
        self._bot_neighborhood_matrix=np.zeros((15, 15), dtype=int)


    def matrices_difference(self):
        '''
        Method is looking for defference between _act_board and _prev_board. Only one difference can be detected.
        ---
        Returns:
        x, y, diff (int, int, int): x position,
                                    y position, 
                                    sign which has appeared -> 0-[err nothing found], 1-'x', 2-'o'
        '''
        diff_matrix=self._act_board-self._prev_board

        try:
            x=int(np.where(diff_matrix>0)[0][0])
            y=int(np.where(diff_matrix>0)[1][0])
            diff=int(diff_matrix[x][y])

            #print(f'x: {x}      y:{y}   diff:{diff}')

            return x, y, diff

        except:
            return False       


    def change_act_board_cell(self, x, y, value):
        '''
        Method change one cell of self._act_board putting a value given by parameter.
        Method FOR DEVELOPMENT PURPOSES!
        ---
        Parameters:
        x (int): 
        y (int):
        value (int): value which is written on selected position, according to formula: 0-'', 1-'x', 2-'o'
        '''
        self._act_board[int(x), int(y)]=int(value)


    def change_prev_board_cell(self, x, y, value):
        '''
        Method change one cell of self._prev_board putting a value given by parameter.
        Method FOR DEVELOPMENT PURPOSES!
        ---
        Parameters:
        x (int): 
        y (int):
        value (int): value which is written on selected position, according to formula: 0-'', 1-'x', 2-'o'
        '''
        self._prev_board[int(x), int(y)]=int(value)


    def is_winner_around(self, x, y, possible_matrix):
        '''
        Method checks if there is winner around chosen cell (because of this cell).
        It looks for series of signs 1-'x' or 2-'o', depends on sign located in cell [x, y]
        ---
        Parameters:
        x (int): 
        y (int):
        possible_matrix (numpy.ndarray): copy of self._act_board with added signs to check if it is winning combination
        ---
        Returns:
        True/False (bool): is winner or not
        '''

        sign=possible_matrix[x][y]

        if sign==1:
            #looking for 'x' as winner
            another_sign=2
        elif sign==2:
            #looking for 'o' as winner
            another_sign=1
        else:
            return False

        signs_matrix=possible_matrix.copy("K") #copy possible_matrix   
        signs_matrix=np.where(signs_matrix==another_sign, 0, signs_matrix) #remove another signs and replace them by 0 value
        signs_matrix=np.where(signs_matrix==sign, 1, signs_matrix) #replace signs by 1 value

        #direction list [x_start, y_start, x_iterator, y_iterator] -> x_start and y_start is relative to x and y
        directions_dictionary={
            "dir0": [4, 0, -1, 0],
            "dir1": [4, -4, -1, 1],
            "dir2": [0, -4, 0, 1],
            "dir3": [-4, -4, 1, 1]            
        }

        # check each direction if there is combination with 5 the same signs in row
        # 20 combination is checked: 5 combination for each direction        
        for direction in directions_dictionary:
            for i in range(5):

                mask_x_start=x+directions_dictionary[direction][0]+i*directions_dictionary[direction][2]
                mask_x_end=mask_x_start+directions_dictionary[direction][2]*4

                mask_y_start=y+directions_dictionary[direction][1]+i*directions_dictionary[direction][3]
                mask_y_end=mask_y_start+directions_dictionary[direction][3]*4

                #print(f"mask_x_start: {mask_x_start}    mask_x_end: {mask_x_end}    mask_y_start: {mask_y_start}    mask_y_end: {mask_y_end}")

                if mask_x_start>=0 and mask_x_start<=14 and mask_x_end>=0 and mask_x_end<=14 and mask_y_start>=0 and mask_y_start<=14 and mask_y_end>=0 and mask_y_end<=14:
                    mask_matrix=np.ones((15, 15), dtype=int)
                    mask_matrix[mask_x_start+directions_dictionary[direction][2]*0][mask_y_start+directions_dictionary[direction][3]*0]=0
                    mask_matrix[mask_x_start+directions_dictionary[direction][2]*1][mask_y_start+directions_dictionary[direction][3]*1]=0
                    mask_matrix[mask_x_start+directions_dictionary[direction][2]*2][mask_y_start+directions_dictionary[direction][3]*2]=0
                    mask_matrix[mask_x_start+directions_dictionary[direction][2]*3][mask_y_start+directions_dictionary[direction][3]*3]=0
                    mask_matrix[mask_x_start+directions_dictionary[direction][2]*4][mask_y_start+directions_dictionary[direction][3]*4]=0

                    #print(f"masked possible matrix elements sum: {np.sum(np.ma.masked_array(possible_matrix, mask_matrix))}")

                    if np.sum(np.ma.masked_array(signs_matrix, mask_matrix))==5:
                        #if there is 5 elements after put mask on signs_matrix this is the winning combination
                        return True

        return False


    def update_directional_risk_matrix_after_opponent_shot(self, x, y):
        '''
        Method updates _directional_risk_matrix taking into consideration opponent shot (x, y) 
        ---
        Parameters:
        x (int):
        y (int):
        '''

        opponent_sign=self._act_board[x][y] #opponent sign

        if opponent_sign==1:
            bot_sign=2
        else:
            bot_sign=1

        #direction dictionary specifying directions parameters: [x_iterator, y_iterator]
        directions_dictionary={
            0: [-1, 0],
            1: [-1, 1],
            2: [0, 1],
            3: [1, 1],
            4: [1, 0],
            5: [1, -1],
            6: [0, -1],
            7: [-1, -1]
        }

        #set zero risk for filled cell
        self._directional_risk_matrix[x][y]=np.zeros((8), dtype=int)

        #check all directions
        for movement_direction in directions_dictionary:
            
            #direction opposite to movement direction
            opposite_direction=movement_direction+4
            if opposite_direction>=8:
                opposite_direction-=8

            for i in range(1,5):
                #check each cell (which can connect with cell [x,y]) in chosen direction
                checked_cell_x=x+directions_dictionary[movement_direction][0]*i
                checked_cell_y=y+directions_dictionary[movement_direction][1]*i

                if checked_cell_x>14 or checked_cell_x<0 or checked_cell_y>14 or checked_cell_y<0:
                    #if want to check outside the board
                    break

                else:
                    #if is inside the board, there are three possible values in checked cell 0->empty, 1-'x', 2-'o'
                    if self._act_board[checked_cell_x][checked_cell_y]==0:
                        #checked cell is empty, increase opposite direction for this cell
                        self._directional_risk_matrix[checked_cell_x][checked_cell_y][opposite_direction]+=1

                    elif self._act_board[checked_cell_x][checked_cell_y]==opponent_sign:
                        #checked cell is filled with opponent sign skip this cell
                        pass

                    elif self._act_board[checked_cell_x][checked_cell_y]==bot_sign:
                        #checked cell is filled with bot sign, skip direction because this sign block influence of opponent_sign
                        break


    def update_directional_risk_matrix_after_bot_shot(self, x, y):
        '''
        Method updates _directional_risk_matrix taking into consideration bot shot (x, y) 
        ---
        Parameters:
        x (int):
        y (int):
        '''

        bot_sign=self._act_board[x][y] #bot sign

        if bot_sign==1:
            opponent_sign=2
        else:
            opponent_sign=1

        #direction dictionary specifying directions parameters: [x_iterator, y_iterator]
        directions_dictionary={
            0: [-1, 0],
            1: [-1, 1],
            2: [0, 1],
            3: [1, 1],
            4: [1, 0],
            5: [1, -1],
            6: [0, -1],
            7: [-1, -1]
        }

        #set zero risk for filled cell
        self._directional_risk_matrix[x][y]=np.zeros((8), dtype=int)

        #check all directions
        for movement_direction in directions_dictionary:
            
            #direction opposite to movement direction
            opposite_direction=movement_direction+4
            if opposite_direction>=8:
                opposite_direction-=8

            encountered_opponent_signs=0

            for i in range(1,5):
                #check each cell (which can connect with cell [x,y]) in chosen direction
                checked_cell_x=x+directions_dictionary[movement_direction][0]*i
                checked_cell_y=y+directions_dictionary[movement_direction][1]*i
                
                if checked_cell_x>14 or checked_cell_x<0 or checked_cell_y>14 or checked_cell_y<0:
                    #if want to check outside the board
                    break

                else:
                    #if is inside the board, there are three possible values in checked cell 0->empty, 1-'x', 2-'o'
                    if self._act_board[checked_cell_x][checked_cell_y]==0:
                        #if cell is empty then set opposit direction to 0+encountered_opponent_signs
                        self._directional_risk_matrix[checked_cell_x][checked_cell_y][opposite_direction]=0+encountered_opponent_signs

                    elif self._act_board[checked_cell_x][checked_cell_y]==opponent_sign:
                        #checked cell is filled with opponent sign, skip this cell but add information about encountered_opponent_signs
                        encountered_opponent_signs+=1
                        pass

                    elif self._act_board[checked_cell_x][checked_cell_y]==bot_sign:
                        #checked cell is filled with bot sign, skip direction because this sign block influence of opponent_sign
                        break


    def update_directional_neighborhood_matrix_after_opponent_shot(self, x, y):
        '''
        Method updates _directional_neighborhood_matrix taking into consideration opponent shot (x, y) 
        ---
        Parameters:
        x (int):
        y (int):
        '''

        #direction dictionary specifying directions parameters: [x_iterator, y_iterator]
        directions_dictionary={
            0: [-1, 0],
            1: [-1, 1],
            2: [0, 1],
            3: [1, 1],
            4: [1, 0],
            5: [1, -1],
            6: [0, -1],
            7: [-1, -1]
        }

        #fill appropriate neighborhood direction for all surrounding cells
        for movement_direction in directions_dictionary:            
            #direction opposite to movement direction
            opposite_direction=movement_direction+4
            if opposite_direction>=8:
                opposite_direction-=8

            neighbor_x=x+directions_dictionary[movement_direction][0]
            neighbor_y=y+directions_dictionary[movement_direction][1]

            if neighbor_x>=0 and neighbor_x<=14 and neighbor_y>=0 and neighbor_y<=14:
                # if neighbor cell is inside board
                self._directional_neighborhood_matrix[neighbor_x][neighbor_y][opposite_direction]=1
            

    def sum_directional_risk_matrix(self):
        '''
        Method sums directional risk for each cell and prints summed matrix
        Method FOR DEVELOPMENT PURPOSES!
        '''

        sumed_risk=np.zeros((15, 15), dtype=int)

        for i in range(15):
            #x change
            for j in range(15):
                #y change
                sumed_risk[i][j]=self._directional_risk_matrix[i][j].sum()

        print(f"SUMMED RISK MATRIX:\n{sumed_risk}")


    def sum_directional_neighborhood_matrix(self):
        '''
        Method sums directional neighborhood for each cell and prints summed matrix
        Method FOR DEVELOPMENT PURPOSES!
        '''        
        sumed_neighborhood=np.zeros((15, 15), dtype=int)

        for i in range(15):
            #x change
            for j in range(15):
                #y change
                sumed_neighborhood[i][j]=self._directional_neighborhood_matrix[i][j].sum()

        print(f"SUMMED NEIGHBORHOOD MATRIX:\n{sumed_neighborhood}")


    def look_for_4th_level_risk(self):
        '''
        Method looks for cells where sum risk of connected directions (direction and opposite_direction) is equal to 4 or more.
        Direction risk is only taken into consideration if there is neighbor in this direction.
        ---
        Returns:
        risks_founded_list (list): list of lists with 4th level risk cells.
                                   Format: [[x1, y1], [x2, y2], ...]     
        '''

        risks_founded_list=[]

        #check all directions in all cells
        for i in range(15):
            for j in range(15):
                for direction in range(4):
                    
                    #opposite direction
                    opposite_direction=direction+4
                    if opposite_direction>=8:
                        opposite_direction-=8

                    if (self._directional_risk_matrix[i][j][direction]*self._directional_neighborhood_matrix[i][j][direction])+(self._directional_risk_matrix[i][j][opposite_direction]*self._directional_neighborhood_matrix[i][j][opposite_direction])>=4:
                        risks_founded_list.append([i, j])
                        print(f"!!!! Risk level 4 founded in cell: x: {i}, y: {j}")

        return risks_founded_list
    

    def look_for_3rd_level_risk(self, bot_sign):
        '''
        Method looks for cells where sum of connected directions (direction and opposite_direction) is equal to 3.
        Cells specified as 3rd level risk can be danger more or less depends on directions.
        Only cells where risk comes from both opposite direction or cells which has pair are taken into consideration
        ---
        Parameter:
        bot_sign (int): 1->'x', 2->'o'
        ---
        Returns:
        risks_founded_list (list): list of lists with 3rd level risk cells.
                                             Format: [[x1, y1], [x2, y2], ...] 
        '''

        #direction dictionary specifying where to look for pair or free space: [relative_x, relative_y, free_cell_relative_x, free_cell_relative_y]
        directions_dictionary={
            0: [-4, 0, -2, 0],
            1: [-4, 4, -2, 2],
            2: [0, 4, 0, 2],
            3: [4, 4, 2, 2],
            4: [4, 0, 2, 0],
            5: [4, -4, 2, -2],
            6: [0, -4, 0, -2],
            7: [-4, -4, -2, -2]
        }

        if bot_sign==1:
            opponent_sign=2
        else:
            opponent_sign=1

        risks_founded_list=[]

        #check all directions in all cells
        for i in range(15):
            for j in range(15):
                for direction in range(4):
                    
                    #opposite direction
                    opposite_direction=direction+4
                    if opposite_direction>=8:
                        opposite_direction-=8

                    if (self._directional_risk_matrix[i][j][direction]*self._directional_neighborhood_matrix[i][j][direction])+(self._directional_risk_matrix[i][j][opposite_direction]*self._directional_neighborhood_matrix[i][j][opposite_direction])==3:
                        # if sum of risk of opposite directions is equal to 3

                        if (self._directional_risk_matrix[i][j][direction]*self._directional_neighborhood_matrix[i][j][direction])>0 and (self._directional_risk_matrix[i][j][opposite_direction]*self._directional_neighborhood_matrix[i][j][opposite_direction])>0:
                            # ----- CASE 1: risk comes from both directions -----

                            if self._directional_risk_matrix[i][j][direction]>self._directional_risk_matrix[i][j][opposite_direction]:
                                # x_checked and y_checked are (for sure) inside board because x_pair and y_pair are inside board
                                x_checked=i+directions_dictionary[direction][2]
                                y_checked=j+directions_dictionary[direction][3]

                                #print(f"x_checked: {x_checked}  y_checked: {y_checked}")
                                
                                if self._act_board[x_checked][y_checked]==opponent_sign:
                                    risks_founded_list.append([i, j])
                                    print(f"!!! Risk (both directions) level 3 founded in cell: x: {i}, y: {j}")

                            else:
                                # x_checked and y_checked are (for sure) inside board because x_pair and y_pair are inside board
                                x_checked=i+directions_dictionary[opposite_direction][2]
                                y_checked=j+directions_dictionary[opposite_direction][3]

                                #print(f"x_checked: {x_checked}  y_checked: {y_checked}") 

                                if self._act_board[x_checked][y_checked]==opponent_sign:
                                    risks_founded_list.append([i, j])
                                    print(f"!!! Risk (both directions) level 3 founded in cell: x: {i}, y: {j}")

                        elif (self._directional_risk_matrix[i][j][direction]*self._directional_neighborhood_matrix[i][j][direction])==0 or (self._directional_risk_matrix[i][j][opposite_direction]*self._directional_neighborhood_matrix[i][j][opposite_direction])==0:
                            # ----- CASE 2: risk comes from one direction -----
                            x_pair=i+directions_dictionary[direction][0]
                            y_pair=j+directions_dictionary[direction][1]
                        
                            if x_pair>=0 and x_pair<=14 and y_pair>=0 and y_pair<=14:
                                # if possible pair is inside board

                                if (self._directional_risk_matrix[x_pair][y_pair][direction]*self._directional_neighborhood_matrix[x_pair][y_pair][direction])+(self._directional_risk_matrix[x_pair][y_pair][opposite_direction]*self._directional_neighborhood_matrix[x_pair][y_pair][opposite_direction])==3:
                                    #pair in checked directions is also equal to 3
                                    risks_founded_list.append([i, j])
                                    print(f"!!! Risk (pair) level 3 founded in cell: x: {i}, y: {j}")

        return risks_founded_list


    def look_for_2nd_level_risk(self, bot_sign):
        '''
        Method looks for cells where sum of connected directions (direction and opposite_direction) is equal to 2 at least twice.
        Function return cells only when directions are not blocked.
        ---
        Parameter:
        bot_sign (int): 1->'x', 2->'o'
        --
        Returns:
        risks_founded_list (list): list of lists with 2nd level risk cells.
                                             Format: [[x1, y1], [x2, y2], ...] 
        '''

        #direction_dictionary specifying where to check risk is relevant: [relative_x, relarive_y]
        directions_dictionary={
            0: [-3, 0, -2, 0],
            1: [-3, 3, -2, 2],
            2: [0, 3, 0, 2],
            3: [3, 3, 2, 2],
            4: [3, 0, 2, 0],
            5: [3, -3, 2, -2],
            6: [0, -3, 0, -2],
            7: [-3, -3, -2, -2]
        }

        if bot_sign==1:
            opponent_sign=2
        else:
            opponent_sign=1


        risks_founded_list=[]

        #check all cells
        for i in range(15):
            for j in range(15):

                #create risk list taking into consideration neighborhood to manage cases better
                neighborhood_risk_list=[]
                for direction in directions_dictionary:
                    neighborhood_risk_list.append(self._directional_risk_matrix[i][j][direction]*self._directional_neighborhood_matrix[i][j][direction])

                if neighborhood_risk_list.count(2)>=2:
                    
                    # ---------- CASE 1: 2x2 ----------
                    direction_2nd_level_risk=[]
                    
                    #find directions where 2nd level risk is present
                    for direction in directions_dictionary:
                        if neighborhood_risk_list[direction]==2:
                            direction_2nd_level_risk.append(direction)

                    #check if found aren't blocked
                    for risky_direction in direction_2nd_level_risk:
                        blocked_directions=0

                        x_block=i+directions_dictionary[risky_direction][0]
                        y_block=j+directions_dictionary[risky_direction][1]

                        if x_block>=0 and x_block<=14 and y_block>=0 and y_block<=14:
                            if self._act_board[x_block][y_block]==bot_sign:
                                #inside board and is blocked
                                blocked_directions+=1
                        else:
                            #blocking cell is outside board, that in fact mean that risk is blocked by board borders
                            blocked_directions+=1

                    #check how many direction is blocked
                    if len(direction_2nd_level_risk)-blocked_directions>=2:
                        risks_founded_list.append([i, j])
                        print(f"!!! Risk (2x2) level 2 founded in cell: x: {i}, y: {j}") 
                  
                elif neighborhood_risk_list.count(2)==1 and neighborhood_risk_list.count(1)==2:
                    # ---------- CASE 2: 1x2 + 2x1 ----------
                    direction_2nd_level_risk=[]
                    direction_1st_level_risk=[]
                    
                    #find directions where 2nd and 1st level risk is present
                    for direction in directions_dictionary:
                        if neighborhood_risk_list[direction]==2:
                            direction_2nd_level_risk.append(direction)
                        elif neighborhood_risk_list[direction]==1:
                            direction_1st_level_risk.append(direction)
                    
                    if abs(direction_1st_level_risk[1]-direction_1st_level_risk[0])==4:
                        # only if direction_1st_level_risk are opposit
                        # it is for sure 2 elements in direction_1st_level_risk because neighborhood_risk_list.count(1)==2

                        #count how many directions are blocked
                        blocked_directions=0

                        for risky_direction in direction_2nd_level_risk:
                            
                            x_block=i+directions_dictionary[risky_direction][0]
                            y_block=j+directions_dictionary[risky_direction][1]

                            if x_block>=0 and x_block<=14 and y_block>=0 and y_block<=14:
                                if self._act_board[x_block][y_block]==bot_sign:
                                    #inside board and is blocked
                                    blocked_directions+=1
                            else:
                                #blocking cell is outside board, that in fact mean that risk is blocked by board borders
                                blocked_directions+=1

                        for risky_direction in direction_1st_level_risk:

                            x_block=i+directions_dictionary[risky_direction][2]
                            y_block=j+directions_dictionary[risky_direction][3]

                            if x_block>=0 and x_block<=14 and y_block>=0 and y_block<=14:
                                if self._act_board[x_block][y_block]==bot_sign:
                                    #inside board and is blocked
                                    blocked_directions+=1
                            else:
                                #blocking cell is outside board, that in fact mean that risk is blocked by board borders
                                blocked_directions+=1

                        #check how many direction is blocked
                        if blocked_directions==0:
                            risks_founded_list.append([i, j])
                            print(f"!!! Risk (1x2 + 2x1) level 2 founded in cell: x: {i}, y: {j}")         

                elif neighborhood_risk_list.count(1)==4:
                    # ---------- CASE 3: 4x1 ----------
                    direction_1st_level_risk=[]

                    #find directions where 1st level risk is present
                    for direction in directions_dictionary:
                        if neighborhood_risk_list[direction]==1:
                            direction_1st_level_risk.append(direction)
                    
                    directions_with_opposit=0
                    #check if directions are opposit
                    for direction in direction_1st_level_risk:

                        opposite_direction=direction+4
                        if opposite_direction>=8:
                            opposite_direction-=8
                        
                        if direction_1st_level_risk.count(opposite_direction)!=0:
                            #if opposite direction is present in direction_1st_level_risk
                            directions_with_opposit+=1

                    if directions_with_opposit==4:
                        #there are 4 directions with opposite (2 pairs) so this is case 3, is needed to find out if directions are blocked

                        #check if founded directions aren't blocked
                        for risky_direction in direction_1st_level_risk:
                            blocked_directions=0

                            x_block=i+directions_dictionary[risky_direction][2]
                            y_block=j+directions_dictionary[risky_direction][3]

                            if x_block>=0 and x_block<=14 and y_block>=0 and y_block<=14:
                                if self._act_board[x_block][y_block]==bot_sign:
                                    #inside board and is blocked
                                    blocked_directions+=1
                            else:
                                #blocking cell is outside board, that in fact mean that risk is blocked by board borders
                                blocked_directions+=1

                        #check how many direction is blocked
                        if blocked_directions==0:
                            risks_founded_list.append([i, j])
                            print(f"!!! Risk (4x1) level 2 founded in cell: x: {i}, y: {j}")   

        return risks_founded_list  


    def update_chance_matrix(self, bot_sign):
        '''
        Method updates _chance_matrix by trying all possible combinations of 2 bot signs which can connect (3 or less cells between).
        First level prediction (first sign) is only checked in cells with bot sign neighborhood.
        ---
        Parameter:
        bot_sign (int): 1->'x', 2->'o'
        '''

        if bot_sign==1:
            opponent_sign=2
        else:
            opponent_sign=1
            
        #direction dictionary specifying directions parameters: [x_iterator, y_iterator]
        directions_dictionary={
            0: [-1, 0],
            1: [-1, 1],
            2: [0, 1],
            3: [1, 1],
            4: [1, 0],
            5: [1, -1],
            6: [0, -1],
            7: [-1, -1]
        }

        self._chance_matrix=np.ones((15, 15, 2), dtype=int)*9

        # reset number_of_occurrences
        for i in range(15):
            for j in range(15):
                self._chance_matrix[i][j][1]=0

        #check all cells
        for x_1st_level in range(15):
            for y_1st_level in range(15):

                if self._act_board[x_1st_level][y_1st_level]==0 and self._bot_neighborhood_matrix[x_1st_level][y_1st_level]!=0:
                    # IF CELL IS EMPTY AND HAS A BOT'S SIGN AS NEIGHBOR
              
                    possible_matrix_1st_level=self._act_board.copy("K")
                    possible_matrix_1st_level[x_1st_level][y_1st_level]=bot_sign

                    if self.is_winner_around(x_1st_level, y_1st_level, possible_matrix_1st_level):
                        # if bot can end game with one shot
                        self._chance_matrix[x_1st_level][y_1st_level][0]=1
                                            
                    else:
                        # if bot can't end game with one shot, check if it's possible with two shots
                        # check all directions
                        for direction in directions_dictionary:
                            for i in range(1, 5):
                                x_2nd_level=x_1st_level+directions_dictionary[direction][0]*i
                                y_2nd_level=y_1st_level+directions_dictionary[direction][1]*i

                                if x_2nd_level>=0 and x_2nd_level<=14 and y_2nd_level>=0 and y_2nd_level<=14:
                                    # if checked cell is inside board
                                    if self._act_board[x_2nd_level][y_2nd_level]==0:
                                        # cell is empty
                                        possible_matrix_2nd_level=possible_matrix_1st_level.copy("K")                                        
                                        possible_matrix_2nd_level[x_2nd_level][y_2nd_level]=bot_sign                                        

                                        if self.is_winner_around(x_2nd_level, y_2nd_level, possible_matrix_2nd_level):
                                            # if two cells are filled bot can win with two shots
                                            if self._chance_matrix[x_1st_level][y_1st_level][0]!=1: # do not change value if 1 is in cell
                                                self._chance_matrix[x_1st_level][y_1st_level][0]=2
                                                self._chance_matrix[x_1st_level][y_1st_level][1]+=1 # increment number_of_occurrences indicator
                                            if self._chance_matrix[x_2nd_level][y_2nd_level][0]!=1: # do not change value if 1 is in cell
                                                self._chance_matrix[x_2nd_level][y_2nd_level][0]=2 
                                                self._chance_matrix[x_2nd_level][y_2nd_level][1]+=1 # increment number_of_occurrences indicator

                                    elif self._act_board[x_2nd_level][y_2nd_level]==bot_sign:
                                        # cell is occupied by bot sign
                                        pass #do nothing

                                    elif self._act_board[x_2nd_level][y_2nd_level]==opponent_sign:
                                        #cell is occupied by opponent sign
                                        break #break checking this direction
                                         
                else:
                    # CELL ISN'T EMPTY
                    self._chance_matrix[x_1st_level][y_1st_level][0]=9


    def look_for_1st_level_chance(self):
        '''
        Method looks for cells where 1st level chance was calculated (win with 1 movements).
        ---
        Returns:
        chance_found_list (list): list of lists with 1st level chance.
                                  Format: [[x1, y1], [x2, y2], ...]
        '''

        chance_found_list=[]

        #check all cells
        for i in range(15):
            for j in range(15):

                if self._chance_matrix[i][j][0]==1:
                    chance_found_list.append([i, j])
                    print(f"@@@@ Chance level 1 found in cell: x: {i}, y: {j}")

        return chance_found_list


    def look_for_2nd_level_chance(self):
        '''
        Method looks for cells where 2nd level chance was calculated (win with 2 movements).
        ---
        Returns:
        chance_found_list (list): list of lists with 2nd level chance.
                                  Format: [[x1, y1], [x2, y2], ...]
        '''
        chance_found_list=[]
        temp_chance_found_list=[]
        max_number_of_occurrences=0

        #check all cells
        for i in range(15):
            for j in range(15):

                if self._chance_matrix[i][j][0]==2:
                    temp_chance_found_list.append([i, j, self._chance_matrix[i][j][1]])
                    
                    if self._chance_matrix[i][j][1]>max_number_of_occurrences:
                        max_number_of_occurrences=self._chance_matrix[i][j][1]

        for element in temp_chance_found_list:
            if element[2]==max_number_of_occurrences:
                chance_found_list.append([element[0], element[1]])
                print(f"@@@@ Chance level 2 (most often occuruing) found in cell: x: {element[0]}, y: {element[1]}")
                    
        return chance_found_list


    def update_bot_neighborhood_matrix_after_bot_shot(self, x, y):
        '''
        Method updates _bot_neighborhood_matrix by setting 1 in surrounding cells after bot shot
        ---
        Parameters:
        x (int): x coordinate of bot shot
        y (int): y coordinate of bot shot
        '''

        #direction dictionary specifying directions parameters: [x_iterator, y_iterator]
        directions_dictionary={
            0: [-1, 0],
            1: [-1, 1],
            2: [0, 1],
            3: [1, 1],
            4: [1, 0],
            5: [1, -1],
            6: [0, -1],
            7: [-1, -1]
        }

        #fill appropriate neighborhood cells for all surrounding cells
        for direction in directions_dictionary:            

            neighbor_x=x+directions_dictionary[direction][0]
            neighbor_y=y+directions_dictionary[direction][1]

            if neighbor_x>=0 and neighbor_x<=14 and neighbor_y>=0 and neighbor_y<=14:
                # if neighbor cell is inside board
                self._bot_neighborhood_matrix[neighbor_x][neighbor_y]=1


    def look_for_cells_next_to_bot_sign(self):
        '''
        Method looks for cells which has bot sign as neighbor.
        ---
        Returns:
        cells_found_list (list): list of lists with cells which have bot sign as neighbor.
                                  Format: [[x1, y1], [x2, y2], ...]
        '''

        cells_found_list=[]

        #check all cells
        for i in range(15):
            for j in range(15):

                if self._act_board[i][j]==0 and self._bot_neighborhood_matrix[i][j]!=0:
                    cells_found_list.append([i, j])

        return cells_found_list


    def look_for_cells_next_to_opponent_sign(self):
        '''
        Method looks for cells which has opponent sign as neighbor.
        ---
        Returns:
        cells_found_list (list): list of lists with cells which have opponent sign as neighbor.
                                  Format: [[x1, y1], [x2, y2], ...]
        '''

        cells_found_list=[]

        #check all cells
        for i in range(15):
            for j in range(15):

                if self._act_board[i][j]==0 and self._directional_neighborhood_matrix[i][j].sum()!=0:
                    cells_found_list.append([i, j])

        return cells_found_list


    def look_for_cells_next_to_bot_and_opponent_signs(self):
        '''
        Method looks for cells which has opponent sign and bot sign as neighbors.
        ---
        Returns:
        cells_found_list (list): list of lists with cells which have opponent sign and bot sign as neighbors.
                                  Format: [[x1, y1], [x2, y2], ...]
        '''
        cells_found_list=[]

        list_of_cells_next_to_opponent_sign=self.look_for_cells_next_to_opponent_sign()
        list_of_cells_next_to_bot_sign=self.look_for_cells_next_to_bot_sign()

        for cell in list_of_cells_next_to_bot_sign:
            if list_of_cells_next_to_opponent_sign.count(cell):
                cells_found_list.append(cell)

        return cells_found_list