import socket
import pickle
import time
import random
import bot_class
import numpy as np


#CLASSES
class game:
    def __init__(self):
        '''
        Create game object
        ---
        Attributes:
        _all_states (dictionary): all game states description
        _state (int): actual game state
        '''
        self._all_states={"idle":0, "I'm_ready":1, "my_shot":2, "opponent_shot":3, "end_of_game":4}
        self._state=0


    def change_state(self, new_state):
        '''
        Method which changes state
        ---
        Parameters:
        new_state (int): activate this state
        '''        
        self._state=new_state


    def convert_board_list_to_matrix(self, board):
        '''
        Method convert board (list) to bord_matrix (numpy.ndarray) which is used by bot object, conversion: ' '->0, 'x'->1, 'o'->2 
        ---
        Parameter:
        board (list):  2-demensional list of strings size 15x15
        ---
        Return:
        board_matrix (numpy.ndarray): 2-demensional numpy.ndarray size 15x15 
        '''
        board_matrix=np.zeros((15, 15), dtype=int)

        for i in range(15):
            # x change
            for j in range(15):
                # y change
                if board[i][j]==' ':
                    board_matrix[i][j]=0
                elif board[i][j]=='x':
                    board_matrix[i][j]=1
                elif board[i][j]=='o':
                    board_matrix[i][j]=2

        return board_matrix


    def shot(self, board, sign):
        '''
        Method which calculates x,y shot coordinate based on actual board
        ---
        Parameters:
        board (list): 2-demensional list of strings size 15x15, it is a board for a game
        sign (string): bot sign: 'x' or 'o'
        ---
        Returns:
        [x y] (list): list of two elements - shot coordinate
            x (int): <0;14>
            y (int): <0;14>
        '''

        # change string sign for bot "numpy" convension: ' '->0, 'x'->1, 'o'->2 
        if sign=='x':
            bot_sign=1
        elif sign=='o':
            bot_sign=2

        board_matrix=game.convert_board_list_to_matrix(board) #converd board for bot
        bot._act_board=board_matrix #update bot._act_board
     
        if bot.matrices_difference():
            # if there is some difference between bot _act_board and _prev_board find the best cell to shot
            
            # RISK, NEIGHBORHOOD, INVERT CHANCE MATRICES PREPARATION
            x, y, diff=bot.matrices_difference() # look for difference
            bot._prev_board=bot._act_board
            print(f"SHOT FUNC DEBUG INFO:   opponent shot    x:{x}  y:{y}")

            bot.update_directional_risk_matrix_after_opponent_shot(x, y)
            bot.update_directional_neighborhood_matrix_after_opponent_shot(x, y)
                 
            
            # CHANCE MATRIX CALCULATION
            bot.update_chance_matrix(bot_sign)

            # CHOOSE SHOT COORDINATES
            still_looking_for_cell=True
            
            # A. End game if possible
            if still_looking_for_cell:
                list_of_1st_level_chance_cells=bot.look_for_1st_level_chance()
                if list_of_1st_level_chance_cells!=[]:
                    chosen_cell=random.choice(list_of_1st_level_chance_cells)
                    x_shot=chosen_cell[0]
                    y_shot=chosen_cell[1]
                    still_looking_for_cell=False
            
            # B. Block 4th level risk
            if still_looking_for_cell:
                list_of_4th_level_risk_cells=bot.look_for_4th_level_risk()
                if list_of_4th_level_risk_cells!=[]:
                    chosen_cell=random.choice(list_of_4th_level_risk_cells)
                    x_shot=chosen_cell[0]
                    y_shot=chosen_cell[1]
                    still_looking_for_cell=False

            # C. End game if possible in 2 movements
            if still_looking_for_cell:
                list_of_2nd_level_chance_cells=bot.look_for_2nd_level_chance()
                if list_of_2nd_level_chance_cells!=[]:
                    chosen_cell=random.choice(list_of_2nd_level_chance_cells)
                    x_shot=chosen_cell[0]
                    y_shot=chosen_cell[1]
                    still_looking_for_cell=False

            # D. Block 3rd level risk
            if still_looking_for_cell:
                list_of_3th_level_risk_cells=bot.look_for_3rd_level_risk(bot_sign)
                if list_of_3th_level_risk_cells!=[]:
                    chosen_cell=random.choice(list_of_3th_level_risk_cells)
                    x_shot=chosen_cell[0]
                    y_shot=chosen_cell[1]
                    still_looking_for_cell=False

            # E. Block 2nd level risk on 2 directions            
            if still_looking_for_cell:
                list_of_2nd_level_risk_cells=bot.look_for_2nd_level_risk(bot_sign)
                if list_of_2nd_level_risk_cells!=[]:
                    chosen_cell=random.choice(list_of_2nd_level_risk_cells)
                    x_shot=chosen_cell[0]
                    y_shot=chosen_cell[1]
                    still_looking_for_cell=False

            # F. Random shot next to opponent and bot
            if still_looking_for_cell:
                list_of_cells_next_to_bot_and_opponent_sign=bot.look_for_cells_next_to_bot_and_opponent_signs()
                if list_of_cells_next_to_bot_and_opponent_sign!=[]:
                    chosen_cell=random.choice(list_of_cells_next_to_bot_and_opponent_sign)
                    x_shot=chosen_cell[0]
                    y_shot=chosen_cell[1]
                    still_looking_for_cell=False

            # F. Random shot next to opponent
            if still_looking_for_cell:
                list_of_cells_next_to_opponent_sign=bot.look_for_cells_next_to_opponent_sign()
                if list_of_cells_next_to_opponent_sign!=[]:
                    chosen_cell=random.choice(list_of_cells_next_to_opponent_sign)
                    x_shot=chosen_cell[0]
                    y_shot=chosen_cell[1]
                    still_looking_for_cell=False
        
            # G. Random shot next to bot
            if still_looking_for_cell:
                list_of_cells_next_to_bot_sign=bot.look_for_cells_next_to_bot_sign()
                if list_of_cells_next_to_bot_sign!=[]:
                    chosen_cell=random.choice(list_of_cells_next_to_bot_sign)
                    x_shot=chosen_cell[0]
                    y_shot=chosen_cell[1]
                    still_looking_for_cell=False

        else:
            # if there is no difference between matrices (bot has to start).
            # Random cell in range x:<5,9>; y:<5,9> is choosen for start    
            while 1:
                x_shot=int(5+round(4*random.random()))
                y_shot=int(5+round(4*random.random()))
                if board[x_shot][y_shot]==' ':
                    break

        bot.update_directional_risk_matrix_after_bot_shot(x_shot, y_shot)
        bot.update_bot_neighborhood_matrix_after_bot_shot(x_shot, y_shot)

        bot._act_board[x_shot, y_shot]=bot_sign
        bot._prev_board=bot._act_board

        return [x_shot, y_shot]



class server:
    def __init__(self, _ip_address, _port):
        '''
        Create server object for communication with a server
        ---
        Attributes:
        _ip_address (string):
        _port (int):
        _client_socket (socket): client socket to send and receive messages
        _all_control (dictionary): all possible control values description
        _server_control (int): last control value received from server
        _server_supp_string (string): last supplementary string value received from server
        _server_board (list): 2-demensional list of strings size 15x15, it is a board received from the server
        '''
        self._ip_address=_ip_address
        self._port=_port
        self._client_socket=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._all_control={"I'm_ready":1, "my_shot":2, "I'm_quitting":3}
        self._server_control=0
        self._server_supp_string=''
        self._server_board=[]


    def set_socket_blockig(self, _bool):
        '''
        Method allows to configure setblocking option for client socket
        ---
        Parameters:
        _bool (bool):   True - activate blocking by the receiving socket
                        False - deactivate blocking by the receiving socket
        '''
        self._client_socket.setblocking(_bool)


    def connect(self):
        '''
        Method connects to server
        ---
        Returns:
        True/False (bool): bool information about connection if it's ok or not
        '''
        try:
            self._client_socket.connect((ip_address_server, port_server))
            return True
        except:
            return False


    def send(self, control=0, supp_string="", shot=[0,0]):
        '''
        Method sends message to server
        ---
        Parameters:
        control (int): integer value specifying message type:
                    1-I'm ready; 2-My shot; 3-I'm quitting
        supp_string (string): string value allows to send supplementary information such player name
        shot (list) -> [x y]: x,y (int)
        '''
        msg=[control, supp_string, shot]
        self._client_socket.send(pickle.dumps(msg))


    def receive(self):
        '''
        Method receives message from server and updates _server_control, _server_supp_string, _server_board
        '''
        msg_recv=pickle.loads(self._client_socket.recv(4096))

        self._server_control=msg_recv[0]
        self._server_supp_string=msg_recv[1]
        self._server_board=msg_recv[2]




if __name__ == "__main__":

    #INPUT server address and username
    ip_address_server=str(input("server ip address: "))

    # static address
    #ip_address_server="192.168.0.129"#temp
    port_server=int(2020)
    username="bot"

    server=server(ip_address_server, port_server)#server init
    connected=server.connect()

    game=game()#game init

    bot=bot_class.bot()#bot init (bot calculates x and y coordinate of bot shot)    

    if connected:
        #connected to server
        server.send(supp_string=username)#send first message containing username
        print("\n****Connected to server****")

        game.change_state(game._all_states["idle"])

        while 1:
            #script loop

            if game._state==0:

                time.sleep(0.2)            
                #Send "ready" information and start the game:
                server.send(control=server._all_control["I'm_ready"])    
                #Go to "I'm_ready" state
                game.change_state(game._all_states["I'm_ready"])
                print("\n--- Ready message was sent ---")
               
            else:
                # others states
                server.receive()#first step is to receive message from server

                # ----- update game._state -----
                if server._server_control==1:
                    game.change_state(game._all_states["I'm_ready"])

                elif server._server_control==2:
                    game.change_state(game._all_states["my_shot"])

                elif server._server_control==3:
                    game.change_state(game._all_states["opponent_shot"])

                elif server._server_control==4 or server._server_control==5:
                    game.change_state(game._all_states["end_of_game"])
                # ------------------------------


                # ----- states reaction -----
                if game._state==1:
                    # "I'm_ready" state
                    print("\n+++ Waiting for opponent +++")

                elif game._state==2:
                    # "my_shot" state
                    print("\n*** Making a shot ***")

                    t0=time.time()                    
                    shot_list=game.shot(server._server_board, server._server_supp_string)
                    t1=time.time()
                    print(f"\nShot calculation time: {round((t1-t0), 2)} [s]")

                    if t1-t0<2:
                        print(f"Wait: {round((2-(t1-t0)), 2)} [s]")
                        time.sleep(2-(t1-t0))#minimum time for bot shot 2s
                    
                    server.send(control=2, shot=shot_list)

                    print(f"^^^ Bot shot (x,y): ({shot_list[0]},{shot_list[1]}) ^^^")

                elif game._state==3:
                    # "opponent_shot" state
                    print("\n*** Opponent shot ***")

                elif game._state==4:
                    # "end_of_game" state
                    print("\n&&& End of game &&&")

                    if server._server_control==4:
                        print("\n#####   I'M THE WINNER !!!   #####")
                    elif server._server_control==5:
                        print("\n#####   I'M THE LOSER -,-   #####")

                    time.sleep(2)                    
                    break
                # ---------------------------

    else:
        #connection error
        print("\nConnection error occured.")

    print("\nQuitting... bye, bye")
    time.sleep(2)