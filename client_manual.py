import socket
import pickle
import time
from tkinter import *
import math
import select


#CLASSES
class game:
    def __init__(self):
        '''
        Create game object
        ---
        Attributes:
        _all_states (dictionary): all game states description
        _state (int): actual game state
        '''
        self._all_states={"idle":0, "I'm_ready":1, "my_shot":2, "opponent_shot":3, "end_of_game":4}
        self._state=0


    def change_state(self, new_state):
        '''
        Method which changes state
        ---
        Parameters:
        new_state (int): activate this state
        '''        
        self._state=new_state


    def shot(self, board):
        '''
        Method which calculates x,y shot coordinate based on actual board
        ---
        Parameters:
        board (list): 2-demensional list of strings size 15x15, it is a board for a game
        ---
        Returns:
        [x y] (list): list of two elements - shot coordinate
            x (int): <0;14>
            y (int): <0;14>
        '''

        #tutaj napisać swój algorytm

        x=int(0)
        y=int(0)

        return [x, y]



class server:
    def __init__(self, _ip_address, _port):
        '''
        Create server object for communication with a server
        ---
        Attributes:
        _ip_address (string):
        _port (int):
        _client_socket (socket): client socket to send and receive messages
        _all_control (dictionary): all possible control values description
        _server_control (int): last control value received from server
        _server_supp_string (string): last supplementary string value received from server
        _server_board (list): 2-demensional list of strings size 15x15, it is a board received from the server
        '''
        self._ip_address=_ip_address
        self._port=_port
        self._client_socket=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._all_control={"I'm_ready":1, "my_shot":2, "I'm_quitting":3}
        self._server_control=0
        self._server_supp_string=''
        self._server_board=[]


    def set_socket_blockig(self, _bool):
        '''
        Method allows to configure setblocking option for client socket
        ---
        Parameters:
        _bool (bool):   True - activate blocking by the receiving socket
                        False - deactivate blocking by the receiving socket
        '''
        self._client_socket.setblocking(_bool)


    def connect(self):
        '''
        Method connects to server
        ---
        Returns:
        True/False (bool): bool information about connection if it's ok or not
        '''
        try:
            self._client_socket.connect((ip_address_server, port_server))
            return True
        except:
            return False


    def send(self, control=0, supp_string="", shot=[0,0]):
        '''
        Method sends message to server
        ---
        Parameters:
        control (int): integer value specifying message type:
                    1-I'm ready; 2-My shot; 3-I'm quitting
        supp_string (string): string value allows to send supplementary information such player name
        shot (list) -> [x y]: x,y (int)
        '''
        msg=[control, supp_string, shot]
        self._client_socket.send(pickle.dumps(msg))


    def receive(self):
        '''
        Method receives message from server and updates _server_control, _server_supp_string, _server_board
        '''
        msg_recv=pickle.loads(self._client_socket.recv(4096))

        self._server_control=msg_recv[0]
        self._server_supp_string=msg_recv[1]
        self._server_board=msg_recv[2]



class board:
    def __init__(self):
        '''
        Constructor create blank board size 15x15
        ---
        Attributes:
        board_list (list): 2-demensional list of strings size 15x15, it is a board for the game
        '''
        self.board_list=[]
        for _ in range(0,15):
            board_row=[]
            for _ in range(0,15):
                board_row.append(' ')
            self.board_list.append(board_row)


    def update_all(self, board):
        '''
        Method update all board:
        ---
        Parameters:
        board (list) -> list of strings size 15x15, actual board from server
        '''
        self.board_list=board


    def find_difference(self, new_board):
        '''
        Method finds first difference between actual board and new_board given as parameter:
        ---
        Parameters:
        new_board (list) -> list of strings size 15x15, actual board from server
        ---
        Returns:
        [pos_x, pos_y, sign] (list) -> pos_x (int) -> x coordinate
                                       pos_y (int) -> y coordinate
                                       sing (string) -> 'o' or 'x'
        '''
        for y in range(len(self.board_list)):
            for x in range(len(self.board_list)):
                if self.board_list[x][y]!=new_board[x][y]:
                    return [x, y, new_board[x][y]]
                
        return [15, 15, ""]



class animation:
    def __init__(self):
        '''
        Create animation object which allow to control tkinter window
        ---
        Attributes:
        _opened (bool) -> specifies if window is open or not
        '''
        self._opened=False
        self.master=None
        self.board_border=30
        self.mesh_size=30
        self.board_size=15
        self.dimension= self.board_size * self.mesh_size + self.board_border

    
    def create_board(self, game, server, board):
        '''
        @ source: KK (modified)
        Method creates tkinter window with empty board and start game_management function.
        ---
        Parameters:
        game (object):
        server (object):
        board (object):
        '''
        self._opened=True

        self.master = Tk()
        self.master.title("Cross and circle game")
        self.canvas = Canvas(self.master, width=self.dimension, height=self.dimension)
        self.canvas.pack()

        self.b1 = Button(self.master, text = "Leave window...") 
        self.b1.pack(fill=BOTH, expand=False, side=BOTTOM)
            
        # draw board
        border = self.board_border / 2
        mesh = self.mesh_size

        for index in range(self.board_size + 1):
            # create vertical lines
            self.canvas.create_line(border + index*mesh, border, border + index*mesh, self.dimension - border)
            # create horizontal lines
            self.canvas.create_line(border, border + index*mesh, self.dimension - border, border + index*mesh)

        self.canvas.bind('<ButtonRelease-1>', lambda e:self.click_button_1(e, game, server, board))
        self.b1.bind("<ButtonRelease-1>", lambda e:self.button_released(e, game, server, board))

        server.set_socket_blockig(True)#disable socket blocking when .recv method is used. This's required for simultaneous tkinter and game_management work
        self.master.after(40, lambda: self.game_management(game, server, board))        
        self.master.mainloop()


    def draw_nought(self, x, y, size, nought_color):
        '''
        @ source: KK (modified)
        Method draws circle on board created by create_board method
        ---
        Parameters:
        x (int): integer value <0;14> which specifies board column
        y (int): integer value <0;14> which specifies board row
        size (int): mesh size
        nought_color (string): hex color specification
        '''
        model_scale=3
        model_width=3
        pos_x=(x+1)*self.mesh_size
        pos_y=(y+1)*self.mesh_size
        rad = int(size/model_scale)

        self.canvas.create_oval(pos_x - rad, pos_y - rad, pos_x + rad, pos_y + rad, width=model_width, outline=nought_color)


    def draw_cross(self, x, y, size, cross_color):
        '''
        @ source: KK (modified)
        Method draws cross on board created by create_board method
        ---
        Parameters:
        x (int): integer value <0;14> which specifies board column
        y (int): integer value <0;14> which specifies board row
        size (int): mesh size
        cross_color (string): hex color specification
        '''
        model_scale=3
        model_width=3
        pos_x=(x+1)*self.mesh_size
        pos_y=(y+1)*self.mesh_size
        rad = int(size/model_scale)

        self.canvas.create_line(pos_x - rad, pos_y - rad, pos_x + rad, pos_y + rad, width=model_width, fill=cross_color)
        self.canvas.create_line(pos_x + rad, pos_y - rad, pos_x - rad, pos_y + rad, width=model_width, fill=cross_color)


    def mark_win_fields(self, board, sign, color):
        '''
        Method draws signs on the winning fields. The fields are marked by server with "#"
        ---
        Parameters:
        board (list): list 15x15 size, winning fields are marked with "#"
        sign (string): sign which will be drawn: "o" or "x"
        color (string): hex color specification
        '''
        fields_list=[]

        for y in range(len(board)):
            for x in range(len(board)):
                if board[x][y]=="#":
                    fields_list.append([x, y])

        for pos in fields_list:
            x=pos[0]
            y=pos[1]

            if sign=='o':
                self.draw_nought(x, y, self.mesh_size, color)

            elif sign=='x':
                self.draw_cross(x, y, self.mesh_size, color)

   
    def click_button_1(self, event, game, server, board):
        '''
        Event method
        Mouse button was pressed and released.
        If it's "my_shot" state, sends message to server with shot coordinates
        ---
        Parameters:
        event (event):
        game (object):
        server (object):
        board (object):
        '''

        if game._state==2:
            #game state: "my_shot"

            x=event.x
            y=event.y
                
            if x<=465 and x>=15 and y<=465 and x>=15:
                #shot inside board
                x_pos=math.floor((x-15)/30)
                y_pos=math.floor((y-15)/30)

                print(f"^^^ Your shot (x,y): ({x_pos},{y_pos}) ^^^")

                server.send(control=server._all_control["my_shot"], shot=[x_pos, y_pos])


    def button_released(self, event, game, server, board):
        '''
        Event method
        "Leave window" button was pressed and released.
        Method closes tkinter window, if button was used during the game, "I'm_quitting" information is sending to server
        ---
        Parameters:
        event (event):
        game (object):
        server (object):
        board (object):
        '''
        
        self.master.after_cancel(self._job)
        self._opened=False
        server.set_socket_blockig(True)#disable non blocking feature of socket

        if game._state==2 or game._state==3:
            print("\n!!! 'Give up' message sent !!!")
            server.send(control=server._all_control["I'm_quitting"])

        #close tkinter window
        self.master.destroy()        


    def game_management(self, game, server, board):
        '''
        Method running when tkinter window is open, allows to manage game states
        --
        Parameters:
        game (object):
        server(object):
        board (object):
        '''

        ready = select.select([server._client_socket], [], [], 0.01) #wait for message with timeout 10ms

        if ready[0]:
            #if message is received
            try:    
                server.receive()

                #STATES MANAGEMENT
                # ********** "Waiting for opponent" message from server **********
                if server._server_control==1:
                    game.change_state(game._all_states["I'm_ready"])
                    print("\n+++ Waiting for opponent +++")

                # ********** "Your shot" message from server **********
                elif server._server_control==2:
                    game.change_state(game._all_states["my_shot"])

                    #---   find difference and draw sign   ---                
                    [x, y, sign]=board.find_difference(server._server_board)

                    if x!=15 and sign=='o':
                        #if pos_x!=15: sign was found and it is 'o'
                        self.draw_nought(x, y, self.mesh_size, "#fa1a0d")

                    elif x!=15 and sign=='x':
                        #if pos_x!=15: sign was found and it is 'x'
                        self.draw_cross(x, y, self.mesh_size, "#3842fd")
                    #------------------------------------------

                    print("\n*** Make a shot ***")

                # ********** "Opponent shot" message from server **********
                elif server._server_control==3:
                    game.change_state(game._all_states["opponent_shot"])

                    #---  find difference and draw sign   ---                
                    [x, y, sign]=board.find_difference(server._server_board)

                    if x!=15 and sign=='o':
                        #if pos_x!=15: sign was found and it is 'o'
                        self.draw_nought(x, y, self.mesh_size, "#fa1a0d")

                    elif x!=15 and sign=='x':
                        #if pos_x!=15: sign was found and it is 'x'
                        self.draw_cross(x, y, self.mesh_size, "#3842fd")
                    #------------------------------------------

                    print("\n*** Opponent shot ***")
                        
                # ********** "You win" message from server **********
                elif server._server_control==4:
                    game.change_state(game._all_states["end_of_game"])
                    server.set_socket_blockig(True)#disable nonblocking feature of socket 
                    print("\n#####   YOU ARE THE WINNER!   #####")
                    self.mark_win_fields(server._server_board, server._server_supp_string, "#47db12")

                # ********** "You lose" message from server **********
                elif server._server_control==5:
                    game.change_state(game._all_states["end_of_game"])
                    server.set_socket_blockig(True)#disable nonblocking feature of socket
                    print("\n#####   YOU ARE THE LOSER!   #####")
                    #server sends player sign if player is the loser then opposit sign should be print on the board
                    if  server._server_supp_string=="o":
                        sign_to_print="x"
                    else:
                        sign_to_print="o"
                        
                    self.mark_win_fields(server._server_board, sign_to_print, "#47db12")

                #update board
                board.update_all(server._server_board)

            except:
                pass

        self._job=self.master.after(40, lambda: self.game_management(game, server, board)) 
        


if __name__ == "__main__":

    #INPUT server address and username
    ip_address_server=str(input("server ip address: "))    
    username=str(input("\nusername: "))

    # static address
    #ip_address_server="192.168.0.129"#temp
    port_server=int(2020)

    server=server(ip_address_server, port_server)#server init
    connected=server.connect()

    board=board()#board init
    game=game()#game init
    
    if connected:
        #connected to server
        server.send(supp_string=username)#send first message with username
        print("\n****Connected to server****")

        time.sleep(0.2)
            
        #Send "ready" information and start the game:
        server.send(control=server._all_control["I'm_ready"])
        print("\n--- Ready message was sent ---")

        animation=animation()

        animation.create_board(game, server, board)

        #Tkinter window is open, game is running
        #After game is end, go further

      

    else:
        #connection error
        print("\nConnection error occured.")

    print("\nQuitting... bye, bye")
    time.sleep(3)