import server_classes
import socket
import pickle
import select
import random

ip_address_server=socket.gethostbyname(socket.gethostname())
port_server=2020

print(f"Server avilable on: {ip_address_server}:{port_server}")

server_socket=socket.socket(socket.AF_INET, socket.SOCK_STREAM)#create socket
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)#allow to reuse socket

server_socket.bind((ip_address_server, port_server))

server_socket.listen()

sockets_list=[server_socket]#all sockets list, contains  also server socket

#create players
player1=server_classes.player('x')
player2=server_classes.player('o')

#create message objects
msg_to_player1=server_classes.msg_to_client()
msg_to_player2=server_classes.msg_to_client()
msg_from_player1=server_classes.msg_to_server()
msg_from_player2=server_classes.msg_to_server()

#create game object
game=server_classes.game()

#create board object
board=server_classes.board()

while True:
    #CONTINUOUS SERVER LOOP
    #-----CAPTURE MESSAGES FROM SOCKETS-----
    read_sockets, _, exception_sockets=select.select(sockets_list, [], sockets_list)#if some socket from socket list "capture" sth

    for notified_socket in read_sockets:
        #check which socket caused action
        if notified_socket==server_socket:
            #someone has connected to server socket "wake up"
            if player1._ip_address=="":
                #if there is no player1 assign new connection to player1
                client_socket, client_address=server_socket.accept()
                player1.set_socket(client_socket)
                player1.set_ip_address(client_address)
                sockets_list.append(player1._socket)

                #Capture player1 name
                msg_recv=pickle.loads(player1._socket.recv(4096))
                msg_from_player1.decompose_msg(msg_recv)

                player1.set_name(msg_from_player1._supp_string)

                #print connection information
                print(f"Player1 connected on {player1._ip_address}")
                print(f"Player1 username: {player1._name}")
                print(f"Player1 sign: {player1._sign}")

            elif player2._ip_address=="":
                #if there is no player2 assign new connection to player2
                client_socket, client_address=server_socket.accept()
                player2.set_socket(client_socket)
                player2.set_ip_address(client_address)
                sockets_list.append(player2._socket)

                #Capture player2 name
                msg_recv=pickle.loads(player2._socket.recv(4096))
                msg_from_player2.decompose_msg(msg_recv)

                player2.set_name(msg_from_player2._supp_string)

                #print connection information
                print(f"Player2 connected on {player2._ip_address}")
                print(f"Player2 username: {player2._name}")
                print(f"Player2 sign: {player2._sign}")

            else:
                client_socket, client_address=server_socket.accept()#if not accept select.select() function is call infinite times
                print("New player try to connect... Two players are connected, can't connect new player.")

        else:
            #other socket wake up - send message or disconnect
            #print(f"notified socked: {notified_socket}")
            try:
                msg_recv=pickle.loads(notified_socket.recv(4096))

                if notified_socket==player1._socket:
                    #player1 sent message
                    msg_from_player1.decompose_msg(msg_recv)
                    msg_from_player1.mark_new_message()#mark new message for game management

                elif notified_socket==player2._socket:
                    #player2 sent message
                    msg_from_player2.decompose_msg(msg_recv)
                    msg_from_player2.mark_new_message()#mark new message for game management

            except:
                #socket disconnect: notfied_socket.recv(4096) give error
                if notified_socket==player1._socket:
                    #if player1 disconnect
                    print(f"Connection from {player1._name}, {player1._ip_address} closed")
                    player1.set_ip_address("")
                    player1.set_name("")
                    sockets_list.remove(player1._socket)
                    player1.set_state(False)#player1 isn't ready because disconnected
                    print("Player1 avilable")  

                elif notified_socket==player2._socket:
                    #if player2 disconnect
                    print(f"Connection from {player2._name}, {player2._ip_address} closed")
                    player2.set_ip_address("")
                    player2.set_name("")
                    sockets_list.remove(player2._socket)
                    player2.set_state(False)#player2 isn't ready because disconnected
                    print("Player2 avilable")  

    for notified_socket in exception_sockets:
        sockets_list.remove(notified_socket)
        if notified_socket==player1._socket:
            #if player1 socket caused exception remove player1
            print(f"Connection from {player1._name}, {player1._ip_address} closed")            
            player1.set_ip_address("")
            player1.set_name("")
            sockets_list.remove(player1._socket)
            player1.set_state(False)#player1 isn't ready because disconnected
            print("Player1 avilable")
            print("exception trace")

        elif notified_socket==player2._socket:
            #if player2 socket caused exception remove player2
            print(f"Connection from {player2._name}, {player2._ip_address} closed")
            player2.set_ip_address("")
            player2.set_name("")
            sockets_list.remove(player2._socket)
            player2.set_state(False)#player2 isn't ready because disconnected
            print("Player2 avilable")  
            print("exception trace")          
    #----------------------------------------
    #print(f'MSG FROM PLAYER1: {msg_from_player1._control} , {msg_from_player1._supp_string} , {msg_from_player1._shot}')
    #print(f'MSG FROM PLAYER2: {msg_from_player2._control} , {msg_from_player2._supp_string} , {msg_from_player2._shot}')



    #-------------GAME MANAGEMENT------------
    if player1._ready==False and (game._state==4 or game._state==5):
        #if player1 disconnect during game: go to end_of_game and mark loser and winner
        game.change_state(game._all_states["end_of_game"])
        player1.set_loser(True)
        player2.set_winner(True)           

    if player2._ready==False and (game._state==4 or game._state==5):
        #if player2 disconnect during game: go to end_of_game and mark loser and winner
        game.change_state(game._all_states["end_of_game"])
        player1.set_winner(True)
        player2.set_loser(True) 

    if player1._ready==False and game._state==1:
        #if player1 disconnect while waiting for opponent
        game.change_state(game._all_states["idle"])

    if player2._ready==False and game._state==2:
        #if player2 disconnect while waiting for opponent
        game.change_state(game._all_states["idle"])
    
    
    #************************** _control==1 **************************
    if msg_from_player1._new_message:
        #player1 sent message
        if msg_from_player1._control==1:
            #I'm ready message
            #---------- idle: 0 ----------
            if game._state==0:
                game.change_state(game._all_states["player1_rdy"])
                player1.set_state(True)
                print("Player1 ready")
            
            #---------- player2_rdy: 2 ----------
            elif game._state==2:
                #if player2 is already rdy
                game.change_state(game._all_states["init"])
                player1.set_state(True)
                print("Player1 ready")            

    if msg_from_player2._new_message:
        #player2 sent message
        if msg_from_player2._control==1:
            #I'm ready message
            #---------- idle: 0 ----------
            if game._state==0:
                game.change_state(game._all_states["player2_rdy"])
                player2.set_state(True)
                print("Player2 ready") 
            
            #---------- player1_rdy: 1 ----------
            elif game._state==1:
                #if player1 is already rdy
                game.change_state(game._all_states["init"])
                player2.set_state(True)
                print("Player2 ready") 
            

    #************************** _control==2 **************************
    if msg_from_player1._new_message:
        #player1 sent message
        if msg_from_player1._control==2:
            #My shot
            #---------- idle: 4 ----------
            #that is case when player1 should make shot
            if game._state==4:
                x=int(msg_from_player1._shot[0])
                y=int(msg_from_player1._shot[1])

                if x<0 or x>14 or y<0 or y>14:
                    #values outside the range
                    print(f"Player1 [{player1._name}] shot outside the range: x={x}, y={y}")
                else:
                    if board.board_list[x][y]==' ':
                        board.update(x, y, player1._sign)
                        game.change_state(game._all_states["player2_shot"])
                        winner, sign, positions_list=board.look_for_winner_around(x, y)

                        print(f"Player1 [{player1._name}] shot: x={x}, y={y}")

                        if winner:
                            #if this shot end the game
                            player1.set_winner(True)
                            player2.set_loser(True)
                            game.change_state(game._all_states["end_of_game"])

                            for position in positions_list:
                                #mark win positions
                                board.update(int(position[0]), int(position[1]), '#')

                        elif board.count_sign(' ')==0:
                            #if board is full player1 and player2 are losers
                            player1.set_loser(True)

                    else:
                        print(f"Player1 [{player1._name}] shot occupied position: x={x}, y={y}")
                
    if msg_from_player2._new_message:
        #player2 sent message
        if msg_from_player2._control==2:
            #My shot
            #---------- idle: 5 ----------
            #that is case when player2 should make shot
            if game._state==5:
                x=int(msg_from_player2._shot[0])
                y=int(msg_from_player2._shot[1])

                if x<0 or x>14 or y<0 or y>14:
                    #values outside the range
                    print(f"Player2 [{player2._name}] shot outside the range: x={x}, y={y}")
                else:
                    if board.board_list[x][y]==' ':
                        board.update(x, y, player2._sign)
                        game.change_state(game._all_states["player1_shot"])
                        winner, sign, positions_list=board.look_for_winner_around(x, y)

                        print(f"Player2 [{player2._name}] shot: x={x}, y={y}")

                        if winner:
                            #if this shot end the game
                            player1.set_loser(True)
                            player2.set_winner(True)
                            game.change_state(game._all_states["end_of_game"])

                            for position in positions_list:
                                #mark win position
                                board.update(int(position[0]), int(position[1]), '#')

                        elif board.count_sign(' ')==0:
                            #if board is full player1 and player2 are losers
                            player1.set_loser(True)

                    else:
                        print(f"Player2 [{player2._name}] shot occupied position: x={x}, y={y}")


    #************************** _control==3 **************************
    if msg_from_player1._new_message:
        #player1 sent message
        if msg_from_player1._control==3:
            #I'm quitting
            #---------- idle: 4 ----------
            #when Player1 is ready and there is no other player ready go to "idle":0 state
            if game._state==1:
                player1.set_state(False)
                game.change_state(game._all_states["idle"])

            #quitting during game
            if game._state==4 or game._state==5:
                player1.set_state(False)
                player1.set_loser(True)
                player2.set_winner(True)
                game.change_state(game._all_states["end_of_game"])

    if msg_from_player2._new_message:
        #player2 sent message
        if msg_from_player2._control==3:
            #I'm quitting
            #---------- idle: 4 ----------
            #when Player2 is ready and there is no other player ready go to "idle":0 state
            if game._state==1:
                player2.set_state(False)
                game.change_state(game._all_states["idle"])

            #quitting during game
            if game._state==4 or game._state==5:
                player2.set_state(False)
                player1.set_winner(True)
                player2.set_loser(True)
                game.change_state(game._all_states["end_of_game"])

    #Reset new message mark!
    msg_from_player1.unmark_new_message()
    msg_from_player2.unmark_new_message()
    
    #////////// init: 3 //////////
    if game._state==3:
        #reset players states
        player1.set_winner(False)
        player2.set_winner(False)

        player1.set_loser(False)
        player2.set_loser(False)

        #reset board
        del board
        board=server_classes.board()
        game.change_state(int(round(random.random()))+4)#go to state "player1_shot":4 or "player2_shot":5
        
        print("##### GAME INITIALIZED #####")
    #----------------------------------------



    #--------------SERVER REPLY--------------
    #_control (int) -> 1-waiting for opponent; 2-your shot; 3-opponent shot; 4-victory; 5-loss
    if game._state==1:
        #player1_rdy
        _control_ply1=int(1)

    elif game._state==2:
        #player2_rdy
        _control_ply2=int(1)

    elif game._state==4:
        #player1_shot
        _control_ply1=int(2)
        _control_ply2=int(3)

    elif game._state==5:
        #player2_shot
        _control_ply1=int(3)
        _control_ply2=int(2)
        
    elif game._state==6:
        #end_of_game
        if player1._loser:
            _control_ply1=int(5)
        elif player1._winner:
            _control_ply1=int(4)

        if player2._loser:
            _control_ply2=int(5)
        elif player2._winner:
            _control_ply2=int(4)

    #pickle and send message
    if player1._ready:
        player1._socket.send((pickle.dumps(msg_to_player1.compose_msg(_control_ply1, player1._sign, board.board_list))))

    if player2._ready:
        player2._socket.send((pickle.dumps(msg_to_player2.compose_msg(_control_ply2, player2._sign, board.board_list))))
    #----------------------------------------



    #--------end_of_game:6 MANAGEMENT--------
    #end_of_game is transitional state similar to idle, but end_of_game need to send winner and loser information so it's can't be skipped before sending
    #////////// end_of_game: 6 //////////
    if game._state==6:
        #reset players readiness 
        if player1._ready:
            player1.set_state(False)

        if player2._ready:
            player2.set_state(False)

        #print end information
        print("##### GAME IS OVER #####")
        if player1._winner:
            print(f"Player1 [{player1._name}] IS THE WINNER!")
            print(f"Board occupation level: {225-board.count_sign(' ')}/225")

        if player2._winner:
            print(f"Player2 [{player2._name}] IS THE WINNER!")
            print(f"Board occupation level: {225-board.count_sign(' ')}/225")

        if player1._loser and player2._loser:
            print("Board is full. There is no winner")        
       
        #go to idle:0 state
        game.change_state(game._all_states["idle"])
    #----------------------------------------
    
    print(f'***** STATE: {game._state} ******\n')

    #print(f'********player1._ready: {player1._ready}')
    #print(f'********player2._ready: {player2._ready}')

    #print(f'////player1: winner/loser: {player1._winner}/{player1._loser}')
    #print(f'////player2: winner/loser: {player2._winner}/{player2._loser}')

    #board.print_board()