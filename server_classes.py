import socket

class board:
    def __init__(self):
        '''
        Constructor create blank board size 15x15
        ---
        Attributes:
        board_list (list): 2-demensional list of strings size 15x15, it is a board for a game
        '''
        self.board_list=[]
        for _ in range(0,15):
            board_row=[]
            for _ in range(0,15):
                board_row.append(' ')
            self.board_list.append(board_row)
    
    def print_board(self):
        '''
        Just print board for development purposes
        '''
        print("**********")
        print("BOARD:")
        for i in range(0,15):
            print(self.board_list[i])
        print("**********")

    def update(self, row, column, sign):
        '''
        Update one element of board
        ---
        Parameters:
        row (int): row of the board
        column (int): column of the board
        sign (string): 'x' or 'o'
        '''
        self.board_list[int(row)][int(column)]=sign

    def look_for_winner_around(self, row, column):
        '''
        Look for winner around a specified point on the board.
        All directions are checking.
        ---
        Parameters:
        row (int): row of the board
        column (int): column of the board
        ---
        Returns:
        winner (bool): winner is found or not
        sign (string): winner sign 'x' or 'y' or '!' in case of error
        positions (list): list of lists ([[x1, y1], [x2, y2], ...]) contains positions which give victory
        '''
        if self.board_list[row][column]=='x' or  self.board_list[row][column]=='o':
            #if pointed area contains 'x' or 'o'
            sign=self.board_list[row][column]
            winner=False
            
            positions=[[row, column]]
            count=1
            #***horizontal direction***
            #forward
            for i in range(1,14):#range from 1 to 14 to find and mark series longer than required 5 signs
                if column+i<=14:
                    if self.board_list[row][column+i]==sign:
                        count+=1
                        positions.append([row, column+i])
                    else:
                        break
                else:
                    break
            #backwawrd
            for i in range(1,14):#range from 1 to 14 to find and mark series longer than required 5 signs
                if column-i>=0:
                    if self.board_list[row][column-i]==sign:
                        count+=1
                        positions.append([row, column-i])
                    else:
                        break
                else:
                    break
            #check number of signs
            if count>=5:
                winner=True
            
            if winner!=True:
                #if winner is not found yet
                positions=[[row, column]]
                count=1
                #***vertical direction***
                #down
                for i in range(1,14):
                    if row+i<=14:
                        if self.board_list[row+i][column]==sign:
                            count+=1
                            positions.append([row+i, column])
                        else:
                            break  
                    else:
                        break
                #up
                for i in range(1,14):#range from 1 to 14 to find and mark series longer than required 5 signs
                    if row-i>=0:
                        if self.board_list[row-i][column]==sign:
                            count+=1
                            positions.append([row-i, column])
                        else:
                            break  
                    else:
                        break

                if count>=5:
                    winner=True

            if winner!=True:
                #if winner is not found yet
                positions=[[row, column]]
                count=1
                #***slant up***
                #forward
                for i in range(1,14):#range from 1 to 14 to find and mark series longer than required 5 signs
                    if row-i>=0 and column+i<=14:
                        if self.board_list[row-i][column+i]==sign:
                            count+=1
                            positions.append([row-i, column+i])
                        else:
                            break  
                    else:
                        break
                #backward                                              
                for i in range(1,14):#range from 1 to 14 to find and mark series longer than required 5 signs
                    if row+i<=14 and column-i>=0:
                        if self.board_list[row+i][column-i]==sign:
                            count+=1
                            positions.append([row+i, column-i])
                        else:
                            break  
                    else:
                        break
                                
                if count>=5:
                    winner=True

            if winner!=True:
                #if winner is not found yet
                positions=[[row, column]]
                count=1
                #***slant down***
                #forward
                for i in range(1,14):#range from 1 to 14 to find and mark series longer than required 5 signs
                    if row+i<=14 and column+i<=14:
                        if self.board_list[row+i][column+i]==sign:
                            count+=1
                            positions.append([row+i, column+i])
                        else:
                            break  
                    else:
                        break
                #backward
                for i in range(1,14):#range from 1 to 14 to find and mark series longer than required 5 signs
                    if row-i>=0 and column-i>=0:
                        if self.board_list[row-i][column-i]==sign:
                            count+=1
                            positions.append([row-i, column-i])
                        else:
                            break  
                    else:
                        break  

                if count>=5:
                    winner=True
            
            return winner, sign, positions
        else:
            #if pointed area doesn't contain 'x' or 'o'
            return False, '!', []

    def count_sign(self, sign):
        '''
        Count number of signs inside board_list
        ---
        Parameters:
        sign (-): value to count, ' ','o','x'
        ---
        Returns:
        amount(int): number of specified signs
        '''
        amount=0
        for line in self.board_list:
            amount+=line.count(sign)
        
        return int(amount)


class player:
    def __init__(self, _sign):
        '''
        Constructor create player
        ---
        Attributes:
        _name (string): player name send by player (client)
        _socket (socket): socket from which player connected
        _ip_address (touple): address information: (ip_address, port)
        _ready (bool): ready to play or not
        _winner (bool): winner flag
        _loser (bool): loser flag
        _sign (string): contains sign wich is used by player: 'x' or y'
        Parameters:
        _sign (string): sign value is assigned during object creation
        '''
        self._name=""
        self._socket=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._ip_address=""
        self._ready=False
        self._winner=False
        self._loser=False
        self._sign=_sign

    def set_name(self, _name):
        '''
        Assign name value
        ---
        Parameters:
        _name (string)
        '''
        self._name=_name
    
    def set_socket(self, _socket):
        '''
        Assign socket
        ---
        Parameters:
        _socket (socket)
        '''
        self._socket=_socket

    def set_ip_address(self, _ip_address):
        '''
        Assign address information -> tuple contains (ip_address, port)
        ---
        Parameters:
        _ip_address (tuple): (ip_address, port)
        '''
        self._ip_address=_ip_address

    def set_state(self, _ready):
        '''
        Assign state (ready or not)
        ---
        Parameters:
        _ready (bool): True->rdy, False->not rdy
        '''
        self._ready=_ready

    def set_winner(self, _bool):
        '''
        Assign winner state
        ---
        Parameters:
        _bool (bool): True->a player is winner, False->a player isn't winner
        '''
        self._winner=_bool

    def set_loser(self, _bool):
        '''
        Assign loser state
        ---
        Parameters:
        _bool (bool): True->a player is loser, False->a player isn't loser
        '''
        self._loser=_bool


class game:
    def __init__(self):
        '''
        Constructor create new game
        ---
        Atributes:
        _all_states (dictionary): all game states description
        _state (int): actual game state
        '''
        self._all_states={"idle":0, "player1_rdy":1, "player2_rdy":2, "init":3, "player1_shot":4, "player2_shot":5, "end_of_game":6}
        self._state=0

    def change_state(self, new_state):
        '''
        Function which changes state
        ---
        Parameters:
        new_state (int): activate this state
        '''        
        self._state=new_state


class msg_to_client:
    def __init__(self):
        '''
        Constructor create message to client
        ---
        Attributes:
        _control (int): integer value specifying message type:
                    1-waiting for opponent; 2-your shot; 3-opponent shot; 4-victory; 5-loss
        _supp_string (string): string value allows to send supplementary information such as opponent name or sign
        _board (list): 15x15 list contains game board
        '''
        self._control=0
        self._supp_string=""
        self._board=[]

    def compose_msg(self, control, supp_string, board):
        '''
        Function prepares object to send, message in form of list is composed here
        ---
        Parameters:
        control (int): message type
        supp_string (string): string value allows to send supplementary information such as opponent name or sign
        board (list): 15x15 list contains actual board state
        ---
        Returns:
        list to send
        '''
        self._control=control
        self._supp_string=supp_string
        self._board=board

        return [self._control, self._supp_string, self._board]
    

class msg_to_server:
    def __init__(self):
        '''
        Constructor create message from client
        ---
        Attributes:
        _control (int): integer value specifying message type:
                    1-I'm ready; 2-My shot; 3-I'm quitting
        _supp_string (string): string value allows to send supplementary information such player name
        _shot (list) -> [x y]: x,y (int) 
        '''
        self._control=0
        self._supp_string=""
        self._shot=[]
        self._new_message=False

    def decompose_msg(self, msg_list):
        '''
        Function which update object with a message from client. The message is standarized list.
        ---
        Parameters:
        msg_list (list): 
                msg_list[0] = control (int): message type
                msg_list[1] = supp_string (string)
                msg_list[2] = shoot (list): [x y] -> x,y (int) shot coordinates
        ---
        Returns:
        self
        '''
        self._control=msg_list[0]
        self._supp_string=msg_list[1]
        self._shot=msg_list[2]

        return self

    def mark_new_message(self):
        '''
        Function mark that new message appear, that's crucial for game management
        ---
        Returns:
        self -> self._new_message is set True
        '''
        self._new_message=True

    def unmark_new_message(self):
        '''
        Function reset new message mark
        ---
        Returns:
        self -> self._new_message is set False
        '''
        self._new_message=False    